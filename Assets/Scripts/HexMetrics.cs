﻿using UnityEngine;

public static class HexMetrics
{
    public const float outerRadius = 10f;
    public const float innerRadius = outerRadius * 0.866025404f;

    public static Vector3[] corners = {
        new Vector3(0f, 0f, outerRadius),
        new Vector3(innerRadius, 0f, 0.5f * outerRadius),
        new Vector3(innerRadius, 0f, -0.5f * outerRadius),
        new Vector3(0f, 0f, -outerRadius),
        new Vector3(-innerRadius, 0f, -0.5f * outerRadius),
        new Vector3(-innerRadius, 0f, 0.5f * outerRadius),

        //IndexOutOfRangeException. This happens because the last triangle tries to fetch a seventh corner,
        //which doesn't exist. Of course it should wrap back and use the first corner for its final vertex.
        //Alternatively, we could duplicate the first corner in HexMetrics.corners,
        //so we don't have to worry about going out of bounds.
        new Vector3(0f, 0f, outerRadius)
    };
}
